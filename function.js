$(document).ready(function() {

   $('button').on('click', function() {
      $(this).removeClass('btn-info').addClass('btn-success').html('T&uuml;r ge&ouml;ffnet');
      
      $.ajax({
         url: "/openDoor.php",
         success: function( data ) {
            $('button').removeClass('btn-success').addClass('btn-info').html('T&uuml;r &ouml;ffnen');
         }
      });      
   });

});
