# Upgrade Anleitung #

Mit Putty auf den Raspberry verbinden ( User: **pi** / Passwort: **raspberry** )

Folgende Befehle ausführen:

	cd /var/www/
	rm index.html
	wget https://bitbucket.org/LSCDuempel/tueroeffner/raw/???/index.html
	wget https://bitbucket.org/LSCDuempel/tueroeffner/raw/???/bootstrap.min.css
	wget https://bitbucket.org/LSCDuempel/tueroeffner/raw/???/door.png
	wget https://bitbucket.org/LSCDuempel/tueroeffner/raw/???/functions.js
	wget https://bitbucket.org/LSCDuempel/tueroeffner/raw/???/jquery-2.1.4.min.js
	wget https://bitbucket.org/LSCDuempel/tueroeffner/raw/???/layout.css


Irgendwo in Windows eine Datei *tueroeffner.bat* anlegen, mit folgendem Inhalt. Wobei **C:\PATH_TO\chrome.exe** individuell an  die Installation angepasst werden muss und **http://192.168.x.x** auf den Raspberry zeigen muss:


	"C:\PATH_TO\chrome.exe" --chrome-frame --app="http://192.168.x.x"

Auf dem Desktop oder im Autostart dann eine Verknüpfung auf die *tueroeffner.bat* anlegen und wenn gewünscht noch das Icon in den Einstellungen ändern.

Danach sollte sich beim Klick auf die Verknüpfung oder beim Start des Computers (Autostart) ein kleines Fenster mit dem Türöffner öffnen, dass dann wie eine echte Windowsanwendung aussieht.

